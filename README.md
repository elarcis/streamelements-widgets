# StreamElements Widgets

A collection of custom widgets for [StreamElements](https://streamelements.com), tailored for [my
channel](https://twitch.tv/elarcis)’s overlay.

They are provided as-is for the curious, and should not be expected, for the most part, to be easily
usable by anyone else, for the following reasons:

- It depends on [FontAwesome 5 Pro](https://fontawesome.com) and
  [DSEG](https://github.com/keshikan/DSEG) being installed as fonts — those are not provided with
  this repo for licensing reasons,
- It is to be paired with [my own remote app](https://gitlab.com/elarcis/elarcis) to be controlled,
- Due to StreamElements’s limited hosting abilities, assets such as images and sounds are sourced
  through StreamElements’s CDN based on my personal upload library, meaning they could break anyday
  when I change them — even though the changes may end up on this repo eventually.

## How it works

Every folder in `src/` is a different widget. Each widget is a collection of HTML, CSS and JS, as
well as a `fields.json` file describing which parameters are exposed to StreamElements’s overlay
editor.

Rollup is tasked with individually building every widget and copying files that don’t need to be
processed (assets) into the `dist/` folder. Each file’s content then needs to be manually
copy-pasted into StreamElements’s overlay editor, as it lacks any automated API to do so.

## How to build

```sh
npm install
npm run build
```
