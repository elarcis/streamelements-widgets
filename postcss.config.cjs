const path = require("path");
const tailwindConfig = require("./tailwind.config.cjs");

module.exports = (context) => ({
  plugins: {
    tailwindcss: Object.assign(tailwindConfig, {
      content: [path.join("src", "widgets", context.options.dir, "*.{html,ts}")],
    }),
    autoprefixer: {},
  },
});
