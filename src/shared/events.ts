import { StreamElements as SE } from "@shared/shims";
import { pluralise } from "./strings";

export function getEventProcessor(handlers: Record<string, (eventArgs: any) => void>) {
  return async function processEvent(type: string, eventArgs: any): Promise<void> {
    const handler = handlers[type];
    if (handler != null) {
      await Promise.resolve();
      handler(eventArgs);
    } else {
      return Promise.reject();
    }
  };
}

export function getCheerData(event: SE.CheerEvent): [string, string] {
  let detail: string;

  if (event.amount > 0) {
    if (event.amount % 1000 === 0) {
      detail = `${(event.amount / 1000).toLocaleString()}k`;
    } else {
      detail = event.amount.toLocaleString();
    }

    return [detail, event.name];
  } else {
    return ["", "Aucun cheer !"];
  }
}

export function getSubData(event: SE.SubEvent): [string, string] {
  if (event.bulkGifted) {
    const amount = event.amount.toLocaleString();
    const tier = (event.tier / 1000).toLocaleString();
    return [`×${amount} T${tier}`, event.name];
  }

  let detail;

  if (event.amount > 1) {
    if (event.amount % 12 === 0) {
      const years = event.amount / 12;
      detail = `${years.toLocaleString()} ${pluralise(years, "an")}`;
    } else {
      detail = `${event.amount.toLocaleString()} mois`;
    }
  } else {
    detail = "";
  }

  return [detail, event.name];
}
