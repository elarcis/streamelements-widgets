export default function getLogger(name: string) {
  return function log(...data: any[]) {
    console.log(`[${name.toLocaleUpperCase()}]`, ...data);
  };
}
