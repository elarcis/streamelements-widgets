export async function timeout(delay: number): Promise<void> {
  return new Promise((resolve) => {
    window.setTimeout(resolve, delay);
  });
}

export function debounce<T extends (...args: any[]) => void>(
  func: T,
  wait: number,
  immediate: boolean,
): T {
  let timeout: number | undefined;
  return function (this: unknown, ...args: any[]) {
    let context = this;
    let later = () => {
      timeout = undefined;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    let callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = window.setTimeout(later, wait);
    if (callNow) {
      func.apply(context, args);
    }
  } as T;
}
