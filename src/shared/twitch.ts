import { escapeRegExp } from "./strings";
import logger from "./logger";

export async function loadEmoticonsIndex(emoticonsUrl: string): Promise<[RegExp, string][]> {
  const log = logger("emoticons");
  log("Building emoticons index:");
  log("  - fetching emoticons data...");
  const emoticonsData = await fetch(emoticonsUrl).then<EmoticonsData>((r) => r.json());
  log("  - building regexes...");
  return Object.values(emoticonsData.emoticons).map((emoticon) => [
    new RegExp(`\\b${escapeRegExp(emoticon.code)}\\b`, "g"),
    emoticon.id,
  ]);
}

export function insertEmoticons(html: string, emoticons: [RegExp, string][], size: 1 | 2 | 3) {
  return emoticons.reduce(
    (red, [regex, iconId]) =>
      red.replace(
        regex,
        `<img src="//static-cdn.jtvnw.net/emoticons/v1/${iconId}/${size}.0" class="emoticon" />`,
      ),
    html,
  );
}

export interface EmoticonsData {
  emoticons: Record<string, Emoticon>;
}

export interface Emoticon {
  code: string;
  id: string;
}
