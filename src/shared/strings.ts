export function pluralise(amount: number, word: string) {
  return `${word}${amount > 1 ? "s" : ""}`;
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
export function escapeRegExp(code: string) {
  return code.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
}
