import { getCheerData, getEventProcessor, getSubData } from "@shared/events";
import { StreamElements as SE } from "@shared/shims";

let eventsLimit = 5,
  userLocale = "fr-FR",
  includeFollowers = true,
  includeRedemptions = true,
  includeHosts = true,
  minHost = 0,
  includeRaids = true,
  minRaid = 0,
  includeSubs = true,
  includeTips = true,
  minTip = 0,
  includeCheers = true,
  minCheer = 0;

let userCurrency: { code: string },
  totalEvents = 0,
  lastHostRaidId: number | null = null,
  lastHostRaidName: string | null = null;

const processEvent = getEventProcessor({
  "kvstore:update": (event: SE.KVStoreEvent<any>) => {
    if (event.data.key === "customWidget.layout.expanded") {
      document.body.style.transform = event.data.value.request
        ? "translateX(0)"
        : "translateX(-69%)";
    }
  },
  follower: (event) => includeFollowers && addEvent("follower", "", event.name),
  redemption: (event) => includeRedemptions && addEvent("redemption", "Redeemed", event.name),
  subscriber: (event) =>
    includeSubs && event.isCommunityGift !== true && addEvent("sub", ...getSubData(event)),
  host: (event) => {
    if (includeHosts && minHost <= event.amount && lastHostRaidName !== event.name) {
      addEvent("host", event.amount.toLocaleString(), event.name);
      lastHostRaidName = event.name;
      lastHostRaidId = totalEvents;
    }
  },
  cheer: (event) =>
    includeCheers && minCheer <= event.amount && addEvent("cheer", ...getCheerData(event)),
  tip: (event) => {
    if (includeTips && minTip <= event.amount) {
      const amount = event.amount.toLocaleString(userLocale, {
        style: "currency",
        minimumFractionDigits: event.amount === parseInt(event.amount) ? 0 : 2,
        currency: userCurrency.code,
      });
      addEvent("tip", amount, event.name);
    }
  },
  raid: (event) => {
    if (includeRaids && minRaid <= event.amount) {
      if (lastHostRaidName === event.name && lastHostRaidId != null) {
        removeEvent(lastHostRaidId);
        lastHostRaidId = null;
      }
      lastHostRaidName = event.name;
      addEvent("raid", event.amount.toLocaleString(), event.name);
    }
  },
});

window.addEventListener("onEventReceived", function (obj: any) {
  if (!obj.detail.event) {
    return;
  }

  if (typeof obj.detail.event.itemId !== "undefined") {
    obj.detail.listener = "redemption-latest";
  }

  const listener = obj.detail.listener.split("-")[0];
  const event = obj.detail.event;

  processEvent(listener, event);
});

window.addEventListener("onWidgetLoad", function (obj: any) {
  const recents = obj.detail.recents as { type: string; createdAt: string }[];

  recents.sort((a, b) => {
    return Date.parse(a.createdAt) - Date.parse(b.createdAt);
  });

  const fieldData = obj.detail.fieldData;
  userCurrency = obj.detail.currency;
  eventsLimit = fieldData.eventsLimit;
  includeFollowers = fieldData.includeFollowers === "yes";
  includeRedemptions = fieldData.includeRedemptions === "yes";
  includeHosts = fieldData.includeHosts === "yes";
  minHost = fieldData.minHost;
  includeRaids = fieldData.includeRaids === "yes";
  minRaid = fieldData.minRaid;
  includeSubs = fieldData.includeSubs === "yes";
  includeTips = fieldData.includeTips === "yes";
  minTip = fieldData.minTip;
  includeCheers = fieldData.includeCheers === "yes";
  minCheer = fieldData.minCheer;

  for (const recent of recents) {
    processEvent(recent.type, recent);
  }
});

function addEvent(type: string, text: string, username: string) {
  totalEvents += 1;

  let elementStr = `
    <div class="event-container" id="event-${totalEvents}">
      <span class="event-image event-${type}"></span>
      <span class="username-container">${username}</span>`;

  if (text !== "") {
    elementStr += `<span class="details-container">${text}</span>`;
  }

  elementStr += `</div>`;

  $(".main-container").prepend(elementStr);

  if (totalEvents > eventsLimit) {
    removeEvent(totalEvents - eventsLimit);
  }
}

function removeEvent(eventId: number) {
  $(`#event-${eventId}`).remove();
}
