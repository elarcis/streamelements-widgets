import { getEventProcessor } from "@shared/events";
import logger from "@shared/logger";
import { type ManagerOptions, type Socket, type SocketOptions } from "socket.io-client";

// Imported via a <script> tag
declare const io: (
  uri: string | Partial<ManagerOptions & SocketOptions>,
  opts?: Partial<ManagerOptions & SocketOptions>,
) => Socket;

const log = logger("wizebot-bridge");

const processEvent = getEventProcessor({
  timer: getPassThroughHandle("timer"),
  layout: handleLayout,
  fauteuil: getPassThroughHandle("fauteuil"),
  quote: getPassThroughHandle("quote"),
});

const WIZEBOT_URL = "https://sockets.wizebot.tv:21001";

window.addEventListener("onWidgetLoad", function (obj: any) {
  log("Initialising the Wizebot Custom Events listener...");
  const fieldData = obj.detail.fieldData;
  openConnection(fieldData.apiKey, fieldData.apiSecret);
});

function handleLayout([layoutCommand]: [string]) {
  if (layoutCommand === "expand") {
    storeEvent("layout.expanded", true);
  } else if (layoutCommand === "collapse") {
    storeEvent("layout.expanded", false);
  }
}

function getPassThroughHandle(eventCode: string) {
  return function (request: any) {
    storeEvent(eventCode, request);
  };
}

function dispatch(event: any) {
  log("Wizebot event received:", event);
  if (event.event_name === "api-call") {
    let eventType: string | undefined, eventData: string[] | object;

    if (typeof event.event_datas === "object") {
      eventType = event.event_datas.kind;
      eventData = event.event_datas.data;
    } else {
      [eventType, ...eventData] = event.event_datas.split(".");
    }

    if (eventType == null) {
      throw new Error("Event type not specified");
    }

    processEvent(eventType, eventData)
      .then(() => log("  - Done processing event"))
      .catch(() => log("  - No handler found for the event"));
  }
}

function openConnection(key: string, secret: string) {
  log("Opening web socket for URL: ", WIZEBOT_URL);

  const socket = io(WIZEBOT_URL, {
    autoConnect: true,
    reconnection: true,
    reconnectionDelay: 500,
    secure: true,
  });

  socket.on("is_connected", () => {
    log("  - Connected! Sending auth message to Wizebot (no response is expected)");
    socket.emit("auth", key, secret);
  });

  socket.on("dispatch", dispatch);
}

function storeEvent<T>(kind: string, request: T) {
  SE_API.store.set(kind, {
    requestedAt: Date.now(),
    request,
  });
}
