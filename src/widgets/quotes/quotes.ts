import { timeout } from "@shared/functions";
import logger from "@shared/logger";
import { insertEmoticons, loadEmoticonsIndex } from "@shared/twitch";
import "./quotes.pcss";

const log = logger("quotes");

interface Quote {
  id: number;
  text: string;
  category: string;
  sender: string;
  created_at: string;
  love: number;
}

let quoteList = Promise.resolve(),
  lastRequestedAt: string,
  emoticonsIndex: [RegExp, string][];

let quoteElem: HTMLElement, quoteDuration: number, emoticonsUrl: string, channelId: string;

window.addEventListener("onEventReceived", (obj: any) => {
  if (!obj.detail.event) {
    return;
  }

  const event = obj.detail.event;
  processEvent(obj.detail.listener, event);
});

window.addEventListener("onWidgetLoad", async (obj: any) => {
  log("Loading widget…");

  const fieldData = obj.detail.fieldData;
  quoteDuration = fieldData.duration;
  emoticonsUrl = fieldData.emoticonsUrl;
  channelId = obj.detail.channel.username;

  const divId = "#quote_div";
  const quoteDiv = document.querySelector<HTMLElement>(divId);
  if (quoteDiv == null) {
    throw new Error(`no quote element found with id ${divId}`);
  }
  quoteElem = quoteDiv;

  if (emoticonsUrl != null) {
    log("URL set for emoticons:", emoticonsUrl);
    emoticonsIndex = await loadEmoticonsIndex(emoticonsUrl);
  } else {
    log("No URL set for emoticons");
    emoticonsIndex = [];
  }

  log("Widget ready!");
});

function addQuote(quote: Quote) {
  quoteList = quoteList
    .then(() => processQuote(quote))
    .then(() => timeout(quoteDuration * 1000))
    .then(hideQuote);
}

async function hideQuote() {
  quoteElem.classList.remove("visible");
  await timeout(1000);
}

function processEvent(type: string, event: any) {
  if (type === "kvstore:update") {
    if (
      event.data.key === "customWidget.quote" &&
      event.data.value.requestedAt !== lastRequestedAt
    ) {
      lastRequestedAt = event.data.value.requestedAt;
      log("quote requested", event.data.value);
      addQuote(event.data.value.request);
    }
  }
}

async function processQuote(quote: Quote) {
  log("Processing quote:");

  if (quote == null) {
    log("  - No quote given. Fetching random quote…");
    quote = await getRandomQuote();
    log("  - Random quote found");
  }

  if (quote == null) {
    throw new Error("No quote given nor random quote found!");
  }

  const text = quote.text;
  const createdAt = new Date(quote.created_at);
  let details = `ajoutée par ${quote.sender}`;

  const dateFormat = new Intl.DateTimeFormat("fr-FR", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
  });

  details += " · " + dateFormat.format(createdAt);

  if (quote.love > 0) {
    details += ` · ♥${quote.love}`;
  }

  log(`  - Displaying quote "${text}"…`);
  await showQuote(text, details);
  log(`  - Quote hidden`);
}

async function getRandomQuote(): Promise<Quote> {
  const data = await fetch(`https://wapi.wizebot.tv/api/quotes/${channelId}/random`).then((r) =>
    r.json(),
  );

  if (data.quotes && data.quotes.length > 0) {
    return data.quotes[0];
  } else {
    throw new Error("No random quote returned");
  }
}

async function showQuote(text: string, details: string) {
  const mainElem = quoteElem.querySelector<HTMLElement>(".text .main")!;
  const detailsElem = quoteElem.querySelector<HTMLElement>(".text .details")!;

  mainElem.textContent = text;
  mainElem.innerHTML = insertEmoticons(mainElem.textContent ?? "", emoticonsIndex, 3);
  detailsElem.textContent = details;

  quoteElem.classList.add("visible");
  await timeout(1000);
}
