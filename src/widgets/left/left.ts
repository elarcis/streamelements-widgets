import { getCheerData, getEventProcessor, getSubData } from "@shared/events";
import logger from "@shared/logger";
import { StreamElements as SE } from "@shared/shims";

const log = logger("left-bar");

let includeLastSub = true,
  includeLastCheer = true;

let seatUserElem: JQuery<HTMLElement>,
  lastCheerElem: JQuery<HTMLElement>,
  lastSubElem: JQuery<HTMLElement>;

const processEvent = getEventProcessor({
  "kvstore:update": (event: SE.KVStoreEvent<any>) => {
    switch (event.data.key) {
      case "customWidget.fauteuil":
        seatUserElem.empty().append(getEventElem("seat", "", event.data.value.request.newUser));
        break;
      case "customWidget.layout.expanded":
        document.body.style.transform = event.data.value.request
          ? "translateX(0)"
          : "translateX(69%)";
        break;
    }
  },
  "cheer-latest": (event: SE.CheerEvent) => {
    if (includeLastCheer && lastCheerElem != null) {
      setElemContent(lastCheerElem, "cheer", getCheerData(event));
    }
  },
  "subscriber-latest": (event: SE.SubEvent) => {
    if (includeLastSub && lastSubElem != null) {
      setElemContent(lastSubElem, "sub", getSubData(event));
    }
  },
});

window.addEventListener("onEventReceived", function (obj: any) {
  if (obj.detail?.event == null) {
    return;
  }

  if ("detail" in obj && typeof obj.detail.event?.itemId !== "undefined") {
    obj.detail.listener = "redemption-latest";
  }

  const event = obj.detail?.event;

  // On traite le cas des subs giftés ici parce qu’on ne
  // peut pas faire la distinction au chargement du widget
  if (event != null && event.isCommunityGift !== true) {
    processEvent(obj.detail.listener, event);
  }
});

window.addEventListener("onWidgetLoad", async function (obj: any) {
  const fieldData = obj.detail.fieldData;
  includeLastSub = fieldData.includeLastSub === "yes";
  includeLastCheer = fieldData.includeLastCheer === "yes";

  lastCheerElem = $(".last-cheer");
  lastSubElem = $(".last-sub");
  seatUserElem = $(".seat-user");

  if (!includeLastCheer) {
    $(".last-cheer-label").remove();
  }
  if (!includeLastCheer) {
    $(".last-sub-label").remove();
  }

  const data = obj.detail.session.data;
  log("session data", data);
  log(data["cheer-latest"]);
  processEvent("cheer-latest", data["cheer-latest"]);
  processEvent("subscriber-latest", data["subscriber-latest"]);

  refreshSeatUser();
});

function getEventElem(type: string, text: string, username: string) {
  let element = `
    <div class="event-container">
      <span class="event-image event-${type}"></span>
      <span class="username-container">${username}</span>`;

  if (text !== "") {
    element += `<span class="details-container">${text}</span>`;
  }

  element += `</div>`;

  return element;
}

async function refreshSeatUser() {
  return SE_API.store.get<{ userName: string }>("fauteuil.occupant").then((seatUser) => {
    if (seatUser != null) {
      seatUserElem.empty().append(getEventElem("seat", "", seatUser.userName));
    } else {
      seatUserElem.empty().append(getEventElem("none", "", "Piquez le fauteuil !"));
    }
  });
}

function setElemContent(elem: JQuery<HTMLElement>, type: string, data: [string, string]) {
  elem.empty().append(getEventElem(type, ...data));
}
