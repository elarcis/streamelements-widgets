import { debounce, timeout } from "@shared/functions";
import "./timer.pcss";

const CUBIC_BEZIER_FUNCTION = "cubic-bezier(0.4, 0, 0.2, 1)";
const ANIM_DURATION = 1000;

class TimerBar {
  visible = false;
  counterSeconds = 0;
  initialSeconds = 0;
  counterInterval: number | undefined;

  containerElem: HTMLElement;
  counterElem: HTMLElement;
  progressElem: HTMLElement;

  constructor(container: HTMLElement) {
    this.containerElem = container;
    this.containerElem.classList.add("ease-in-out");
    this.containerElem.style.transform = "scale(0, 0)";
    this.containerElem.style.transitionProperty = "transform";
    this.containerElem.style.transitionDuration = `${ANIM_DURATION}ms`;

    this.progressElem = this.containerElem.appendChild(document.createElement("div"));
    this.progressElem.classList.add("bar-progress", "bar-red");
    this.progressElem.style.transitionProperty = "width";
    this.progressElem.style.width = "0%";

    this.containerElem.appendChild(
      (() => {
        const diamond = document.createElement("div");
        diamond.classList.add("bar-diamond", "bar-red");
        return diamond;
      })(),
    );

    this.containerElem.style.transitionProperty = "transform";
    this.containerElem.style.transform = "scale(0, 0)";
    this.counterElem = this.containerElem.appendChild(document.createElement("div"));
    this.counterElem.style.transitionDuration = `${ANIM_DURATION}ms`;
    this.counterElem.style.transform = "scale(0, 0)";
    this.counterElem.classList.add(
      "bar-counter",
      "absolute",
      "ease-in-out",
      "ml-4",
      "text-5xl",
      "text-lrs-red-500",
    );
  }

  animateCounter(targetSeconds: number, _durationMillis: number) {
    const startSeconds = this.counterSeconds;

    if (this.counterInterval != null) {
      clearInterval(this.counterInterval);
      this.counterInterval = undefined;
    }

    this.progressElem.style.transitionDuration = `${ANIM_DURATION / 2}ms`;
    this.progressElem.style.transitionTimingFunction = "linear";

    this.counterInterval = window.setInterval(() => {
      if (this.counterSeconds === targetSeconds) {
        clearInterval(this.counterInterval);
      } else {
        this.setCounter(this.counterSeconds - 1);
        this.setProgress(this.counterSeconds, startSeconds);
      }
    }, 1000);
  }

  async emptyBar() {
    this.progressElem.style.transitionTimingFunction = CUBIC_BEZIER_FUNCTION;
    this.progressElem.style.transitionDuration = `${ANIM_DURATION}ms`;
    this.progressElem.style.width = "1px";
    await timeout(ANIM_DURATION);
  }

  async fillUpBar() {
    this.progressElem.style.transitionTimingFunction = CUBIC_BEZIER_FUNCTION;
    this.progressElem.style.transitionDuration = `${ANIM_DURATION}ms`;
    this.progressElem.style.width = "100%";
    await timeout(ANIM_DURATION);
  }

  getLoadingDurationMillis() {
    return ANIM_DURATION * 2 + 1000;
  }

  async hide() {
    if (!this.visible) {
      return;
    }

    clearInterval(this.counterInterval);
    this.counterInterval = undefined;
    this.counterElem.style.transform = "scale(0, 0)";
    this.visible = false;
    await this.emptyBar();

    this.counterElem.style.display = "none";
    this.containerElem.style.transform = "scale(0, 0)";
    await timeout(ANIM_DURATION);
  }

  /**
   * @param durationSeconds {number} in seconds
   */
  async play(durationSeconds: number) {
    clearInterval(this.counterInterval);

    this.initialSeconds = durationSeconds;
    await this.show(durationSeconds);

    this.animateCounter(0, durationSeconds * 1000);
    return timeout(durationSeconds * 1000);
  }

  async show(durationSeconds: number) {
    this.visible = true;
    this.containerElem.style.transform = "scale(1, 1)";
    await timeout(ANIM_DURATION);

    this.setCounter(durationSeconds);
    // this.animateCounter(durationSeconds, ANIM_DURATION);
    this.counterElem.style.display = "";
    this.counterElem.style.transform = "scale(1, 1)";
    await this.fillUpBar();
  }

  setCounter(seconds: number) {
    this.counterSeconds = seconds;

    const textContent = (
      [
        [365, ":"],
        [24, ":"],
        [60, ":"],
        [60, ":"],
        [1, ""],
      ] as const
    ).reduce(
      (red, step, i, all) => {
        const stepInSeconds = step[0] * all.slice(i + 1).reduce((prev, cur) => prev * cur[0], 1);

        const stepValue = Math.floor(red.rest / stepInSeconds);
        if (this.initialSeconds >= stepInSeconds) {
          red.str += `${stepValue.toString().padStart(red.pad, "0")}${step[1]}`;
        }
        red.rest %= stepInSeconds;

        red.pad = Math.ceil(Math.log10(step[0]));
        return red;
      },
      { str: "", rest: seconds, pad: 0 },
    ).str;

    if (this.counterElem.textContent !== textContent) {
      this.counterElem.textContent = textContent;
    }
  }

  setProgress(counterSeconds: number, startSeconds: number) {
    this.progressElem.style.width = `${(counterSeconds / startSeconds) * 100}%`;
  }
}

let timerbar: TimerBar;
let activeTimestamp: number | null;
const debounceTimer = debounce(timer, 1000, true);
const debounceCountdown = debounce(countdown, 1000, true);

window.addEventListener("onEventReceived", async function (obj: any) {
  if (!obj.detail.event) {
    return;
  }

  const event = obj.detail.event;
  const listener = obj.detail.listener;

  if (listener === "kvstore:update" && event.data.key === "customWidget.timer") {
    const { absolute, target } = event.data.value.request;

    if (absolute === true) {
      const targetTime = parseLocalTimeParam(target);
      debounceCountdown(targetTime);
    } else {
      const [hours, minutes, seconds] = parseTimeString(target, true);
      debounceTimer(hours * 3600 + minutes * 60 + seconds);
    }
  }
});

window.addEventListener("onWidgetLoad", function () {
  const container = document.querySelector<HTMLElement>(".bar-container");
  if (container == null) {
    throw new Error("no container found");
  }
  timerbar = new TimerBar(container);
});

function countdown(targetDate: number) {
  const duration = (targetDate - Date.now() - timerbar.getLoadingDurationMillis()) / 1000;
  timer(duration);
}

/** @param targetTime Hours:minutes[:seconds] */
function parseLocalTimeParam(targetTime: string) {
  const targetDate = new Date();

  const [hours, minutes, seconds] = parseTimeString(targetTime);

  targetDate.setHours(hours);
  targetDate.setMinutes(minutes);
  targetDate.setSeconds(seconds ?? 0);

  if (targetDate.valueOf() <= Date.now()) {
    targetDate.setDate(targetDate.getDate() + 1);
  }

  return targetDate.valueOf();
}

/**
 * @param time `Hours:minutes`, `Minutes:seconds` or `Hours:minutes:seconds`
 * @param prioritiseSeconds if `true`, prioritise `minutes:seconds` in case a value is missing
 */
function parseTimeString(time: string, prioritiseSeconds: true): [number, number, number];
function parseTimeString(
  time: string,
  prioritiseSeconds?: boolean,
): [number, number] | [number, number, number];
function parseTimeString(
  time: string,
  prioritiseSeconds = false,
): [number, number] | [number, number, number] {
  const values = time.split(":", 3).map((n) => Number.parseInt(n, 10) || 0);

  if (prioritiseSeconds && values.length === 2) {
    values.unshift(0);
  }

  return values as [number, number] | [number, number, number];
}

/** @param duration in seconds */
async function timer(duration: number) {
  if (duration === 0) {
    activeTimestamp = null;
    timerbar.hide();
    return;
  }

  activeTimestamp = Date.now();
  const localTimestamp = activeTimestamp;
  await timerbar.play(duration);
  await timeout(2000);

  if (localTimestamp === activeTimestamp) {
    timerbar.hide();
  }
}
