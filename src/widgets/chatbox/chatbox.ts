import "nearest-color";
import "./chatbox.pcss";

const colorPalette = {
  0: "#817",
  1: "#a35",
  2: "#c66",
  3: "#e94",
  4: "#ed0",
  5: "#9d5",
  6: "#4d8",
  7: "#2cb",
  8: "#0bc",
  9: "#09c",
  10: "#36b",
  11: "#639",
} as const;

const nickColor = {
  colors: colorPalette,
  NICK_OPACITY: 0.9,
  replaceColor: window.nearestColor.from(colorPalette),
} as const;

// Please use event listeners to run functions.
document.addEventListener("onLoad", function () {
  // obj will be empty for chat widget
  // this will fire only once when the widget loads

  const chatlog = document.querySelector("#log") as HTMLElement;
  const observer = new MutationObserver(onMutation);
  observer.observe(chatlog, { childList: true });
});

let lastNick = "";
let lastNickChange = Date.now();

const pronouns = getPronouns();
const PRONOUNS_FR = {
  any: "Tous pronoms",
  hehim: "Il",
  heshe: "Il/Elle",
  hethem: "Il/Iel",
  other: "Autre",
  sheher: "Elle",
  shethem: "Elle/Iel",
  theythem: "Iel",
} as Record<string, string>;
let userPronouns = {} as Record<string, string | null | undefined>;

function onMutation(mutations: MutationRecord[]) {
  for (const mutation of mutations) {
    if (mutation.addedNodes.length) {
      const addedNodesArray = Array.from(mutation.addedNodes);
      const addedDivs = addedNodesArray.filter((node) => node.nodeName === "DIV");

      if (addedDivs.length) {
        const chatDiv = addedDivs.pop() as HTMLElement;
        const message = (chatDiv.querySelector(".message") as HTMLElement).innerText.trim();
        procEnlargeEmoteMessages(chatDiv, message);

        const metaDiv = chatDiv.querySelector(".meta") as HTMLElement;
        const nickDiv = metaDiv.querySelector(".name") as HTMLElement;
        procAddSeparator(metaDiv);
        procChangeColor(metaDiv, nickDiv);

        const nick = nickDiv.innerText;
        procAddPronoun(metaDiv, nick);
        procChainMessages(chatDiv, nick);

        removeOobMessages();
      }
    }
  }
}

//#region Message processing

function procAddPronoun(metaDiv: HTMLElement, nick: string) {
  getUserPronoun(nick.toLocaleLowerCase()).then((pronoun) => {
    if (pronoun != null) {
      applyPronoun(metaDiv, pronoun);
    }
  });
}

function procAddSeparator(metaDiv: HTMLElement) {
  const badgesDiv = metaDiv.querySelector(".badges");
  const separatorDiv = document.createElement("span");
  separatorDiv.classList.add("separator");
  metaDiv.insertBefore(separatorDiv, badgesDiv);
}

function procChainMessages(chatDiv: HTMLElement, nick: string) {
  if (nick !== lastNick || Date.now() - lastNickChange > 5 * 60 * 1000) {
    chatDiv.classList.add("sl__chat__message--new-nick");
    lastNick = nick;
    lastNickChange = Date.now();
  }
}

function procChangeColor(metaDiv: HTMLElement, nickDiv: HTMLElement) {
  const oldColor = metaDiv.style.color;
  const newColor = nickColor.replaceColor(oldColor).value || nickColor.colors[0];
  metaDiv.style.setProperty("--nick-color", newColor);
  metaDiv.style.color = "";
  nickDiv.style.color = hex_inverse_bw(newColor, nickColor.NICK_OPACITY);
}

function procEnlargeEmoteMessages(chatDiv: HTMLElement, message: string) {
  const isEmote = message === "";
  const isEmoji = isOnlyEmoji(message);

  if (isEmoji || isEmote) {
    chatDiv.classList.add("sl__chat__message--emote");
  }

  if (isEmote) {
    chatDiv.querySelectorAll<HTMLElement>(".emote").forEach((emoteDiv) => {
      const emoteImage = emoteDiv.querySelector("img") as HTMLImageElement;
      const newEmoteUrl = emoteImage.src.replace(/\d\.0$/, "3.0");
      emoteImage.src = newEmoteUrl;
      emoteDiv.style.backgroundImage = `url("${newEmoteUrl}")`;
    });
  }
}

function removeOobMessages() {
  const messages = document.querySelectorAll<HTMLElement>(".sl__chat__message");

  for (const message of Array.from(messages)) {
    const bounding = message.getBoundingClientRect();
    // Keep a few messages outside of boundaries for when messages are moderated
    if (bounding.bottom <= -500) {
      message.remove();
    } else {
      // Once inside the viewport, all further messages are inside of it as well.
      break;
    }
  }
}

//#endregion

//#region Pronouns

async function pronounsApi(...path: ["pronouns"]): Promise<{ name: string; display: string }[]>;
async function pronounsApi(...path: ["users", string]): Promise<{ pronoun_id: string }[]>;
async function pronounsApi(...path: string[]) {
  const result = await fetch(`https://pronouns.alejo.io/api/${path.join("/")}`);
  return result.json();
}

function applyPronoun(metaDiv: HTMLElement, pronoun: string) {
  const pronounDiv = document.createElement("div");
  pronounDiv.classList.add("pronoun");
  pronounDiv.innerText = pronoun;
  insertAfter(metaDiv, (parent) => parent.querySelector(".name") as HTMLElement, pronounDiv);
}

async function getPronouns() {
  const pronouns = await pronounsApi("pronouns");
  return pronouns.reduce((acc, curr) => {
    acc[curr.name] = translatePronoun(curr.name) ?? curr.display;
    return acc;
  }, {} as Record<string, string>);
}

async function getUserPronoun(user: string) {
  const savedPronoun = userPronouns[user];

  if (savedPronoun == null) {
    const [userPronoun] = await pronounsApi("users", user);
    userPronouns[user] = userPronoun != null ? (await pronouns)[userPronoun.pronoun_id] : null;
  }

  return userPronouns[user];
}

function translatePronoun(pronounId: string) {
  return PRONOUNS_FR[pronounId] ?? null;
}

//#endregion

//#region Utils

function insertAfter(
  parent: HTMLElement,
  query: (e: HTMLElement) => HTMLElement,
  newNode: HTMLElement,
) {
  const target = query(parent);
  if (target != null) {
    const nextElement = target.nextElementSibling;
    if (nextElement != null) {
      parent.insertBefore(newNode, nextElement);
    } else {
      parent.appendChild(newNode);
    }
  }
}

function isOnlyEmoji(message: string) {
  // https://stackoverflow.com/a/64007175
  const ranges = [
    "\\p{Extended_Pictographic}",
    "\\u200d",
    "\\s", // Also allow whitespace
  ].join("|");

  return new RegExp(`^(${ranges})+$`, "gu").test(message);
}

function hex_to_rgb(hex: string) {
  // https://stackoverflow.com/a/49092130
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex) as string[];
  if (!result) {
    const shorthand = /^#?([a-f\d])([a-f\d])([a-f\d])$/i.exec(hex) as string[];
    result = shorthand.map((value) => `${value}${value}`);
  }
  return result != null
    ? {
        r: parseInt(result[1] ?? "", 16),
        g: parseInt(result[2] ?? "", 16),
        b: parseInt(result[3] ?? "", 16),
      }
    : null;
}

function hex_inverse_bw(hex: string, opacity: number): string {
  // https://stackoverflow.com/a/49092130
  let rgb = hex_to_rgb(hex);
  if (rgb != null) {
    let luminance = 0.2126 * rgb["r"] + 0.7152 * rgb["g"] + 0.0722 * rgb["b"];
    return luminance < 140 ? `rgba(255, 255, 255, ${opacity})` : `rgba(0, 0, 0, ${opacity})`;
  }
  return "";
}

//#endregion
