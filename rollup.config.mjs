import nodeResolve from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";
import { join, parse } from "path";
import { defineConfig } from "rollup";
import copy from "rollup-plugin-copy";
import postcss from "rollup-plugin-postcss";
import prettier from "rollup-plugin-prettier";

export default [
  rollupWidget("chatbox/chatbox.ts"),
  rollupWidget("hexagons/hexagons.pcss"),
  rollupWidget("left/left.ts"),
  rollupWidget("quotes/quotes.ts"),
  rollupWidget("right/right.ts"),
  rollupWidget("timer/timer.ts"),
  rollupWidget("wizebot-bridge/bridge.ts"),
];

/** @param {string} widgetPath */
function rollupWidget(widgetPath) {
  const { dir, base } = parse(widgetPath);
  return defineConfig({
    input: {
      [dir]: join("src", "widgets", dir, base),
    },
    context: "window",
    output: {
      compact: true,
      dir: join("dist", dir),
    },
    plugins: [
      nodeResolve(),
      typescript(),
      prettier({ parser: "babel" }),
      postcss({
        extract: true,
        config: {
          ctx: { dir },
        },
      }),
      copy({
        targets: [
          {
            src: ["src", "widgets", dir, `*.{html,json,css}`].join("/"),
            dest: ["dist", dir].join("/"),
          },
        ],
      }),
    ],
  });
}
